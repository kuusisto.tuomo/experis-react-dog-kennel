// Counter state
// Increment (+1)
// Decrement (-1)
// Boost ( payload: value )

export const increment = () => ({
    type: "[counter] INCREMENT"
})

export const decrement = () => ({
    type: "[counter] DECREMENT"
})
export const boost = (offset = 1) => ({
    type: "[counter] BOOST",
    payload: offset
})
