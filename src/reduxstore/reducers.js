export function counterReducer(state = 0, action){
    switch(action.type){
        case "[counter] INCREMENT":
            return state + 1;
        case "[counter] DECREMENT":
            return state - 1;
        case "[counter] BOOST":
            return state + action.payload;
        default:
            return state;
    }
}