import {boost} from "./actions";

export const counterMiddleware = ({dispatch}) => next => action => {
    next(action);

    // Logical stuff
    if(action.type === "[counter] BOOST"){
        setTimeout(() => {
                console.log("Boosting counter");
                dispatch( boost(900) );
            }, 1000)
    }
}