1. Install Redux, toolkit, browser extension ✅
2. Create a store ✅
3. Create a provider for the store ✅
4. Create actions ✅
5. Create a reducer (based on the actions) ✅
6. Add reducer to our store ✅
7. Testing (browser extension) ✅
8. Create a component ✅
9. Add a selector to the component (getter) ✅
10. Add a dispatch to the component (set state to whatever we want) ✅
11. Create 1 middleware
12. Done!