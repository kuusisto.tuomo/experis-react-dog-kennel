import {applyMiddleware, combineReducers, createStore} from "redux";
import {counterReducer} from "./reducers";
import {composeWithDevTools} from "@redux-devtools/extension";
import {counterMiddleware} from "./middleware";

const appReducer = combineReducers({
    counter: counterReducer
})

const middleWares = applyMiddleware(
    counterMiddleware
)

const store = createStore(appReducer, composeWithDevTools(middleWares));

export default store;