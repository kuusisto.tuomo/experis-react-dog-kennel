import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import DogProvider from './hoc/DogContext';
import {initialize} from "./keycloak/keycloak";
import {Provider} from "react-redux";
import store from "./reduxstore/store";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <h1>Please wait for Keycloak to start...</h1>
    </React.StrictMode>
);
initialize().then(() => {
    root.render(
        <React.StrictMode>
            <Provider store={store}>
                <DogProvider>
                    <App/>
                </DogProvider>
            </Provider>
        </React.StrictMode>
    );
}).catch((error) => {
    root.render(
        <React.StrictMode>
            <h1>Keycloak failed to start. Please refresh and try again or report to admin.</h1>
        </React.StrictMode>
    )
});
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();