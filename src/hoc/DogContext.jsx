import { createContext, useState } from "react";

export const DogContext = createContext(null)

const DogProvider = ({ children }) => {
    const [listDogs, setListDogs] = useState([])

    const addDogToList = async () => {
        try {
            await Promise.all([
                fetch("https://random.dog/woof.json"), // image_response
                fetch("https://randomuser.me/api/")    // info_response
            ]).then(
                responses => {
                    const newDog = { id: listDogs.length + 1 }
                    responses[0].json()
                        .then( j => {
                            if (j.url.includes(".mp4")) {
                                return "./images/kennel.png"
                            } else {
                                return j.url;
                            }
                        }).then( r => newDog.picture = r)
                        .then( () => responses[1].json() )
                        .then( j => j.results[0] )
                        .then( o => {return {
                            name: o.name.first,
                            country: o.location.country,
                            city: o.location.city
                        }})
                        .then( r => {return {...newDog, ...r }})
                        .then( x => setListDogs([...listDogs, x]) )
                })
        } catch (error) { console.error(error) }
    }

    return (
        <DogContext.Provider value={ { listDogs, setListDogs, addDogToList } }>
            { children }
        </DogContext.Provider>
    )
}

export default DogProvider;