import keycloak from "../keycloak/keycloak";
import {NavLink} from "react-router-dom";

function LoggedInRoute({children}){
    if(keycloak.authenticated){ // Is logged in
        return (
            <>{children}</>
        )
    } else{
      return(
          <div>
              <h3>You are not authorized to see this page.</h3>
              <NavLink to={"/"}>Return Home</NavLink>
          </div>
      )
    }
}

export default LoggedInRoute;