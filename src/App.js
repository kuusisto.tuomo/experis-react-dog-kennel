import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import './App.css';
import Catalog from './components/Catalog';
import DogList from './components/DogList';
import TokenPage from "./components/TokenPage";
import keycloak from "./keycloak/keycloak";
import LoggedInRoute from "./hoc/LoggedInRoute";
import Counter from "./components/Counter";
import DogDetails from "./components/DogDetails";

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <h1>Happy Dog Kennel 🐕</h1>
                <div className='dog-gifs'>
                    { [0,1,2,3,4,5].map( (number) => <img src="./images/dog_jump.gif" width={50} alt={`dog_${number}`} key={`dog_${number}`}></img> )}
                </div>
                <nav>
                    <li><Link to="/list">Go to Dog List 🐕</Link> </li>
                    <li><Link to="/catalog">Go to Catalog 🖼</Link> </li>
                    <li><Link to="/details">Go to Details 🐕</Link> </li>
                    <li><Link to="/counter">Go to counter 🧮</Link> </li>
                    <li><Link to="/token">Go to Token 💠</Link> </li>
                    {!keycloak.authenticated
                      ?  <li><button onClick={() => {keycloak.login()}}>Login</button></li>
                      :  <li><button onClick={() => {keycloak.logout()}}>Logout</button></li>
                    }
                    { keycloak.authenticated && keycloak.tokenParsed &&
                        <li>Hello <i>{ keycloak.tokenParsed.name }</i>!</li>
                    }
                </nav>
                <Routes>
                    <Route path="/list" element= { <DogList /> } />
                    <Route path="/catalog" element= { <Catalog />  } />
                    <Route path="/details" element= { <DogDetails />  } />
                    <Route path="/counter" element={ <Counter/> }/>
                    <Route path="/token" element= {
                        <LoggedInRoute>
                        <TokenPage />
                        </LoggedInRoute>
                    }/>
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;