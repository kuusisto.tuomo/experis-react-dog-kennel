import { useContext } from "react";
import { DogContext } from "../hoc/DogContext";

function Catalog () {

    const { listDogs } = useContext(DogContext)

    return(
        <>
            <h2>Dog Catalog</h2>
            <p>Number of dogs: <b>{ listDogs.length }</b></p>
        </>
    )
}

export default Catalog;