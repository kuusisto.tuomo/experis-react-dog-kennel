function DogListItem (props) {
    // Do all the const definitions/destruction, prop gets
    const { name, country, city, picture, id } = props.dog

    return (
        <div className="grid-container">
            <div className="grid-item">
                <img src={picture} alt="Not found" width="200px"></img>
            </div>
            <div className="grid-item">
                <div>
                    <span>ID: {id}</span>
                </div>
                <p>Hello my name is</p>
                <h2>{name}</h2>
                <p>I live in <b>{city + ", " +country}</b></p>
            </div>
        </div>
    )
}

export default DogListItem;