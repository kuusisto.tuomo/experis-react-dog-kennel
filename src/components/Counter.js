import {useDispatch, useSelector} from "react-redux";
import {getCounter} from "../reduxstore/selectors";
import {boost, decrement, increment} from "../reduxstore/actions";

function Counter(){
    const counter = useSelector(getCounter); // or useSelector(state => state.counter)
    const dispatch = useDispatch();
    const handleIncrement = () => dispatch( increment() );
    const handleDecrement = () => dispatch( decrement() );
    const handleBoost = () => dispatch( boost(5) );

    return (
        <>
            <h2>Counter Page</h2>
            <div>
                <span>Current value of [Counter] is: <b>{counter}</b></span>
            </div>
            <div>
                <button onClick={handleIncrement}>Increase</button>
                <button onClick={handleDecrement}>Decrease</button>
                <button onClick={handleBoost}>Boost</button>
            </div>
        </>
    )
}

export default Counter;