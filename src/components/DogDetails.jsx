import {
    useContext, useState,
    // useRef,
    // useState
}
    from "react";
// import {useForm} from "react-hook-form";
import {DogContext} from "../hoc/DogContext";
import {useForm} from "react-hook-form";

function DogDetails(){
    // // Controlled
    // const [name, setName] = useState("");
    // const handleNameChange = event => {
    //     setName(event.target.value);
    // }
    //
    // // Uncontrolled
    // const [fact, setFact] = useState("");
    // const factRef = useRef();
    // const handleFactCommit = event => {
    //     event.preventDefault();
    //     setFact(factRef.current.value);
    // }
    //
    // // Hook form
    // const [important, setImportant] = useState({
    //     username: '',
    //     password: ''
    // })
    // const {register, handleSubmit, formState: { errors } } = useForm();
    // const onFormHookSubmit = data => {
    //     setImportant({...important, ...data});
    // }

    const {listDogs, setListDogs} = useContext(DogContext);
    const [selectedDog, setSelectedDog] = useState(); // Controlled
    const { register,
        handleSubmit,
        // formState:{error}
    } = useForm();

    const handleDogSelected = event => { // Controlled
        event.preventDefault();
        setSelectedDog(event.target.value);
    }

    const onEditSubmit = data => {
        let { name, city, country } = data;
        name = name !== "" ? name : theDog.name;
        city = city !== "" ? city : theDog.city;
        country = country !== "" ? country : theDog.country;

        // eslint-disable-next-line eqeqeq
        const filteredDogList = listDogs.filter(d => d.id != selectedDog)
        const updatedDogList = [...filteredDogList, {...theDog, name, city, country}];
        setListDogs(updatedDogList);
    }

    // eslint-disable-next-line eqeqeq
    const theDog = selectedDog ? listDogs.filter(d => d.id == selectedDog)[0] : undefined;

    return (
            <div>
                <h2>My Dog Form</h2>
                {listDogs.length > 0
                    ?
                    <div> {/* Controlled component */}
                        <span>Select a dog to edit:</span>
                        <select name={"dog-selector"}
                                onChange={handleDogSelected}
                                value={selectedDog}>
                            {listDogs.map((dog, index) => <option value={dog.id} key={index}>{dog.name}</option>)}
                        </select>
                    </div>
                    :
                    <div>
                        <span>No dogs available currently.</span>
                    </div>
                }
            { theDog &&
                <div name={"edit-form"}> { /* Hook form */ }
                    <div>
                        <img src={theDog.picture} alt="dog" width={"200px"}></img>
                        <div>
                            <span>Name </span>
                            <input name={"name"} {...register("name")} placeholder={theDog.name}/>
                        </div>
                        <div>
                            <span>City </span>
                            <input name={"city"} {...register("city")} placeholder={theDog.city}/>
                        </div>
                        <div>
                            <span>Country </span>
                            <input name={"country"} {...register("country")} placeholder={theDog.country}/>
                        </div>
                        <button onClick={handleSubmit(onEditSubmit)}>Confirm edit</button>
                    </div>
                </div>
            }
            </div>

        //     {/*<div> /!* Controlled */}
        //     {/*    <span>Name: </span>*/}
        //     {/*    <input name={"name"} onChange={handleNameChange}></input>*/}
        //     {/*    {name.length > 0 && <p>Current Name: {name}</p>}*/}
        //     {/*</div>*/}
        //     {/*<div> /!* Uncontrolled */}
        //     {/*    <span>Dog Fact: </span>*/}
        //     {/*    <input name={"fact"} ref={factRef}></input>*/}
        //     {/*    <button onClick={handleFactCommit}>Commit Fact</button>*/}
        //     {/*    {fact.length > 0 && <p>Fact: {fact}</p>}*/}
        //     {/*</div>*/}
        //     {/*<div> /!* Hook form */}
        //     {/*    <span>Username</span>*/}
        //     {/*    <input name={"username"} {...register("username", {required: true})}></input>*/}
        //     {/*    {errors.username && <p>Username is required.</p>}*/}
        //     {/*    <span>Password</span>*/}
        //     {/*    <input defaultValue={"hunter2"} name={"password"} {...register("password")}></input>*/}
        //     {/*    <button onClick={handleSubmit(onFormHookSubmit)}>Commit Data</button>*/}
        //     {/*    {important.username.length > 0 && <p>Username: {important.username}</p>}*/}
        //     {/*    {important.password.length > 0 && <p>Password: {important.password}</p>}*/}
        //     {/*</div>*/}
        // {/*</div>*/}
    )
}

export default DogDetails;