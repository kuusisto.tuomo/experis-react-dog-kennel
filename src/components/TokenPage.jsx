import keycloak from "../keycloak/keycloak";
import "./TokenPage.css"

function TokenPage(){
    // console.log(keycloak.tokenParsed);
    return (
        <div>
            <h2>Token page</h2>
            <div>
                <h3>User Roles:</h3>
                <ul>
                    {keycloak.tokenParsed.realm_access.roles.map(text => (
                                <li>{text}</li>
                            ))
                    }
                </ul>
            </div>
            <pre>{keycloak.token}</pre>
        </div>
    )
}

export default TokenPage;